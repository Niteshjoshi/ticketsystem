class Ticket < ActiveRecord::Base
	validates :name, :seat_id_seq, :address, :price_paid, :email_address, :phone , presence: true ,length: {minimum: 3}
end
