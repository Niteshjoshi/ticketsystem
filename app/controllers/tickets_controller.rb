class TicketsController < ApplicationController
 
  def index
    @tickets=Ticket.all
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @ticket=Ticket.find(params[:id])
  end

  # GET /tickets/new
  def new
    @ticket=Ticket.new
  end

  # GET /tickets/1/edit
  def edit
     @ticket=Ticket.find(params[:id])
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    if @ticket.save
    redirect_to action: :show, :id => @ticket.id
    else 
    render 'new'
    end  
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    @ticket = Ticket.find(params[:id])
 
    if @ticket.update(ticket_params)
    redirect_to tickets_path(@ticket.id)  
    else
    render 'edit'
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket=Ticket.find(params[:id])
      @ticket.destroy
      redirect_to :action => 'index'
  end

  private
    
    def ticket_params
      params.require(:ticket).permit(:name, :seat_id_seq, :address, :price_paid, :email_address, :phone)
    end
end
